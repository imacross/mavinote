import ClassComponent from "./components/ClassComponent"
import NoteComponent from "./components/NoteComponent"
import ProfileComponent from "./components/ProfileComponent"

export const routes = [
    {path:"/" , component : ClassComponent},
    {path :"/NoteComponent", component : NoteComponent},
    {path: "/ProfileComponent", component:ProfileComponent}
]